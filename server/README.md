# Testing
Die Anwendung benötigt `youtube-dl`, `ffmpeg` und `pyhon`. Der Testcode ist auch in der Lage die Integration mit diesen Bibliotheken zu überprüfen. Aus diesem Grund liegt im `./test` Folder ein Dockerfile. Mit diesem Dockerfile lässt sich eine Virtuelle Maschine erzeugen, mit der sich der (Unit-)Testcode platformunabhängig ausführen lässt. 

Hierfür muss selbstverständlich [Docker](https://docs.docker.com/install/) installiert sein. 

## Testumgebung erzeugen
Der Befehl `docker build -t tinf17a/aswe:test -f test/Dockerfile .` erzeugt ein Docker Image, von dem ausgehend wir unsere Testumgebungen (Docker container) erzeugen können. Das Docker Image labeln wir mit dem Namen `tinf17a/aswe:test`, damit wir es einfach zuordnen können.

## Testumgebung starten
Für das Testing wird der Sourcecode der Anwendung in das `/home/node/app` Verzeichnis der Virtuellen Maschine eingebunden.
Gestartet wird die Testumgebung (Docker container) mit `docker run -ti -v <Sourcecode Verzeichnis>:/home/node/app -p 8080:8080 --name tinf17a tinf17a/aswe:test`

## In Testumgebung wechseln
Um die Tests auf der Virtuellen Maschine (Docker container) auszuführen, verbinden wir uns über das Terminal mit dem root-User des container `docker exec -ti tinf17a bash`. Unseren Container haben wir zuvor mit `tinf17a` bezeichnet.

## Dependencies installieren
Für das Testing ist wichtig, dass die `node_modules` über die Testumgebung (Docker container) installiert sind.
Ggf. kommt es zu Problemen, wenn mit einem Windows Betriebssystem entwickelt wird. Entfernt darum ggf. zuerst die `node_modules` aus dem Sourcecode.

Unsere Anwendung ist über das Verzeichnis `/home/node/app` verfügbar.

Installiert hier in diesem Verzeichnis die Dependencies über die Testumgebung (Docker container) mit dem Befehl `npm install`

## Tests ausführen
Die Tests lassen sich nun mit `npm run test` bzw. `npm run test:watch` ausführen. 
Letzterer Befehl lässt den Testcode automatisch durchlaufen, sobald sich etwas im JavaScript-Code geändert hat.
