/* Internal member variables */
var http // Reference to http server
var fs // Reference to filesystem

/**
 * Method starts http api
 */
var start = () => {
  http.createServer(function (req, res) {
    if (req.url === '/2639388b95ec2d74902d7559492e78c9') {
      res.writeHead(200)

      // Create a stream to send our song
      const stream = fs.createReadStream('./audio/song.mp3')
      stream.pipe(res)
    } else {
      res.writeHead(404)
      res.end()
    }
  }).listen(8080)
}

/**
 * Factory method
 */
module.exports = () => {
  // Inject dependencies
  http = require('http')
  fs = require('fs')

  return {
    start
  }
}
