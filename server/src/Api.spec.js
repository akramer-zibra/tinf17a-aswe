/* eslint-env mocha */
/* eslint-disable max-len */
/* eslint-disable max-statements */
/* eslint-disable max-lines-per-function */
var expect = require('chai').expect
var mockery = require('mockery')
var sinon = require('sinon')

describe('Api', function () {
  /* Define stubs */
  var routeFunc // We need this to call it separately
  var httpStub = {
    createServer: (func) => {
      routeFunc = func // Cache route function in a variable
      return serverStub
    }
  }

  var serverStub = {
    listen: (port) => {}
  }

  var reqStub = {
    url: '/test'
  }

  var resStub = {
    writeHead: (code) => {},
    end: () => {}
  }

  var fsStub = {
    createReadStream: (path) => {
      return streamStub
    }
  }

  var streamStub = {
    pipe: (res) => {}
  }
  /* */

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: true,
      warnOnUnregistered: false,
      useCleanCache: true
    })

    // Allow source under test module
    mockery.registerAllowable('./Api')
  })

  afterEach(function () {
    mockery.deregisterAll()
    mockery.disable()

    sinon.restore() // Restore sinon default sandbox
  })

  it('start() creates a http server listening on port 8080', function (done) {
    // Create a spy for
    var serverListenSpy = sinon.spy(serverStub, 'listen')

    // Change dependencies with mockery
    mockery.registerMock('http', httpStub)
    mockery.registerMock('fs', fsStub)

    // Initialize api with fresh dependencies
    var api = require('./Api')()

    // Run method to test
    api.start()

    expect(serverListenSpy.calledOnce).to.be.equal(true)
    done()
  })

  it('sends song file with correct route', function (done) {
    // Create a spy for response stub
    var resStubSpy = sinon.spy(resStub, 'writeHead').withArgs(200)
    var streamStubSpy = sinon.spy(streamStub, 'pipe').withArgs(resStub)

    // Change dependencies with mockery
    mockery.registerMock('http', httpStub)
    mockery.registerMock('fs', fsStub)

    // Initialize api with fresh dependencies
    var api = require('./Api')()

    // Run method to test
    api.start()

    // Call the server with /test route
    reqStub.url = '/2639388b95ec2d74902d7559492e78c9'
    routeFunc(reqStub, resStub)

    expect(resStubSpy.calledOnce).to.be.equal(true)
    expect(streamStubSpy.calledOnce).to.be.equal(true) // Is only true, if pipe is called with response stub
    done()
  })

  it('sends 404 on wrong route', function (done) {
    // Create a spy for
    var responseStubSpy = sinon.spy(resStub, 'writeHead').withArgs(404)

    // Change dependencies with mockery
    mockery.registerMock('http', httpStub)
    mockery.registerMock('fs', fsStub)

    // Initialize api with fresh dependencies
    var api = require('./Api')()

    // Run method to test
    api.start()

    // Call the server with /test route
    reqStub.url = '/test'
    routeFunc(reqStub, resStub)

    expect(responseStubSpy.calledOnce).to.be.equal(true) // Is only true, if its called with 404 argument
    done()
  })
})
