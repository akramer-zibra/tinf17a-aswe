/* eslint-env mocha */
/* eslint-disable max-len */
/* eslint-disable max-lines-per-function */
/* eslint-disable max-statements */
/* eslint-disable max-statements-per-line */
var expect = require('chai').expect
var mockery = require('mockery')
var sinon = require('sinon')

describe('Wecksong routine', function () {
  /* Define stubs */
  var weatherStub = {
    currentWeatherDescription: () => {
      return new Promise((resolve, reject) => resolve('sonnig'))
    }
  }

  //
  var spotifyStub = {
    songs: () => {
      return new Promise((resolve, reject) => resolve(['Titel', 'Titel #2', 'Titel #3']))
    }
  }

  //
  var youtubeStub = {
    videos: () => {
      return new Promise((resolve, reject) => resolve([{ title: 'Titel', description: 'description', url: 'https://www.youtube.com/watch?v=znPOJgSgIII' }]))
    },
    download: () => {
      return new Promise((resolve, reject) => resolve(true))
    }
  }

  //
  var pubsubStub = {
    subscribe: (channel, handler) => {},
    publish: (channel, data) => {}
  }
  /* */

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: true,
      warnOnUnregistered: false,
      useCleanCache: true
    })

    // Allow source under test module
    mockery.registerAllowable('./Wecksong')
  })

  afterEach(function () {
    mockery.deregisterAll()
    mockery.disable()
  })

  it('creates an interval timer which triggers every 15 minutes', function (done) {
    // Use fake timers
    var clock = sinon.useFakeTimers()

    // Create a spy for event publish
    var eventPublishSpy = sinon.spy(pubsubStub, 'publish')

    // Register stubs
    mockery.registerMock('../Integrations/Weather', () => { return weatherStub })
    mockery.registerMock('../Integrations/Spotify', () => { return spotifyStub })
    mockery.registerMock('../Integrations/YouTube', () => { return youtubeStub })
    mockery.registerMock('pubsub-js', pubsubStub)

    // Initialize unit under test with its factory method
    require('./Wecksong')()

    expect(eventPublishSpy.calledOnce).to.be.equal(false)

    // Jump to future
    clock.tick(15 * 60 * 1000 + 1000) // 15 minutes + 1s

    expect(eventPublishSpy.calledOnce).to.be.equal(true)
    expect(eventPublishSpy.getCall(-1).firstArg).to.be.equal('wecksong.timer.start')
    done()
  })
})
