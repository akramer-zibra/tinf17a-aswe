/* eslint max-lines-per-function: ["error", 45] */

/**
 * Factory method for the "wecksong" routine
 */
module.exports = () => {
  // Inject module dependencies
  var weather = require('../Integrations/Weather')()
  var spotify = require('../Integrations/Spotify')()
  var youtube = require('../Integrations/YouTube')()

  // Inject pubsub event broker
  var events = require('pubsub-js')

  // Listen to "start" alarm
  events.subscribe('wecksong.timer.start', (msg, data) => {
    weather.currentWeatherDescription().then(weatherDescription => { // Retrieve weather description
      events.publish('wecksong.weather.retrieved', weatherDescription) // ..publish weather description
    })
  })

  // Listen to "weather" description
  events.subscribe('wecksong.weather.retrieved', (msg, data) => {
    spotify.songs(data).then(songs => { // Retrieve song titles from spotify
      events.publish('wecksong.spotify.retrieved', songs) // ..publish song titles
    })
  })

  // Listen to "song titles"
  events.subscribe('wecksong.spotify.retrieved', (msg, data) => {
    youtube.videos(data[0]).then(videos => { // Retrieve some videos
      events.publish('wecksong.youtube.videos.retrieved', videos) // ..publish videos
    })
  })

  // Listen to "videos"
  events.subscribe('wecksong.youtube.videos.retrieved', (msg, data) => {
    youtube.download(data[0].url).then(success => { // Download audio from youtube video
      events.publish('wecksong.youtube.song.downloaded', data[0].url) // ...publish
    })
  })

  // Start routine timer with interval
  setInterval(() => {
    events.publish('wecksong.timer.start', {})
  }, 1000 * 60 * 15) // should trigger every 15 minutes
}
