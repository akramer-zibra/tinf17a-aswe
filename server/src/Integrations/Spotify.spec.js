/* eslint-env mocha */
/* eslint-disable max-len */
/* eslint-disable max-lines-per-function */
var expect = require('chai').expect
var mockery = require('mockery')

beforeEach(function () {
  mockery.enable({
    warnOnReplace: true,
    warnOnUnregistered: false,
    useCleanCache: true
  })

  // Allow source under test module
  mockery.registerAllowable('./Spotify')
})

afterEach(function () {
  mockery.deregisterAll()
  mockery.disable()
})

describe('Spotify integration', function () {
  // Unit under test
  var spotify

  // Define a stub for spotify web api and replace it with mockery
  var spotifyWebApiNodeStub = function SpotifyWebApi (options) {
    return {
      clientCredentialsGrant: () => {
        return new Promise((resolve, reject) => resolve({ body: { access_token: '' } }))
      },
      setAccessToken: (token) => {},
      searchTracks: (words) => {
        return new Promise((resolve, reject) => {
          resolve(require('../../test/fixtures/spotify.response.json'))
        })
      }
    }
  }
  mockery.registerMock('spotify-web-api-node', spotifyWebApiNodeStub)

  beforeEach(() => {
    // Reinitialize our unit under test
    spotify = require('./Spotify')()
  })

  it('retrieves some songs for a weather situation', function (done) {
    spotify.songs('bewölkt').then(songs => {
      expect(songs).to.have.lengthOf.above(0)
      done()
    }).catch(err => {
      expect.fail(err)
      done()
    })
  })
})
