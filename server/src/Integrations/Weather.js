
/** Internal member variables */
var weather // open weather api client

/**
 * Retrieves current weather from Open Weather API
 */
var currentWeather = () => {
  // Call open weather api and retrieve a json result
  return new Promise((resolve, reject) => {
    weather.getSmartJSON((err, result) => {
      if (err) reject(err)
      resolve(result)
    })
  })
}

/**
 * Retrieves a description of current weather
 * @returns A Promise with
 */
var currentWeatherDescription = () => {
  return new Promise((resolve, reject) => {
    currentWeather().then(result => resolve(result.description))
  })
}

/**
 * Factory method
 */
module.exports = () => {
  // Inject dependencies
  weather = require('openweather-apis')

  // Configure weather api client
  weather.setAPPID(process.env.API_OPENWEATHERMAP_APIKEY)
  weather.setLang('de')
  weather.setUnits('metric')
  weather.setCityId(2825297)

  return {
    currentWeather,
    currentWeatherDescription
  }
}
