/* eslint-env mocha */
/* eslint-disable max-len */
/* eslint-disable max-lines-per-function */
/* eslint-disable max-statements */
/* eslint-disable max-statements-per-line */
var expect = require('chai').expect
var mockery = require('mockery')
var sinon = require('sinon')

describe('YouTube integration', function () {
  // Define unit under test
  var youtube

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: true,
      warnOnUnregistered: false,
      useCleanCache: true
    })

    // Allow source under test module
    mockery.registerAllowable('./YouTube')
  })

  afterEach(function () {
    mockery.deregisterAll()
    mockery.disable()
  })

  it('finds videos for given artist and song title', function (done) {
    // Define stub and replace with mockery
    var popytStub = {
      YouTube: function YouTube (key, options) {
        return {
          searchVideos: (words, limit) => {
            return new Promise((resolve, reject) => { resolve(require('../../test/fixtures/youtube.response.json')) })
          }
        }
      }
    }
    mockery.registerMock('popyt', popytStub)

    // Reinitialize unit under test
    youtube = require('./YouTube')()

    //
    youtube.videos('Scotty Fries Sea of Clouds').then(videos => {
      expect(videos).to.be.lengthOf.above(0)
      done()
    }).catch(err => {
      expect.fail(err)
      done()
    })
  })

  it('retrieves video information with title, description and url', function (done) {
    // Define stub and replace with mockery
    var popytStub = {
      YouTube: function YouTube (key, options) {
        return {
          searchVideos: (words, limit) => {
            return new Promise((resolve, reject) => { resolve(require('../../test/fixtures/youtube.response.json')) })
          }
        }
      }
    }
    mockery.registerMock('popyt', popytStub)

    // Reinitialize unit under test
    youtube = require('./YouTube')()

    //
    youtube.videos('Scotty Fries Sea of Clouds').then(videos => {
      expect(videos[0]).to.be.an('object').to.have.all.keys('title', 'description', 'url')
      done()
    }).catch(err => {
      expect.fail(err)
      done()
    })
  })

  it('downloads audio track by a given video URL', function (done) {
    // Define stub and replace with mockery
    var stub = {
      exec: (command, ready) => {
        ready(false, 'result', undefined)
      }
    }
    var childProcessSpy = sinon.spy(stub, 'exec') // Use a spy wrapper
    mockery.registerMock('child_process', stub)

    // Reinitialize unit under test
    youtube = require('./YouTube')()

    //
    var songUrl = 'https://www.youtube.com/watch?v=znPOJgSgIII'

    youtube.download(songUrl).then(info => {
      expect(childProcessSpy.calledOnce).to.be.equal(true)
      expect(childProcessSpy.getCall(-1).firstArg).to.have.string(songUrl) // Check if song url is part of youtube-dl command
      done()
    }).catch(err => {
      expect.fail(err)
      done()
    })
  })
})
