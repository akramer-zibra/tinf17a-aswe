/** Internal member variables */
var spotifyApi // Spotify API integration

/**
 * Extracts songs with artist and title
 * This method also removes duplicates
 * @param {*} response
 * @returns {Array<string>} A collection with strings representing each song received from the api call
 */
var extractSongs = (response) => {
  return response.body.tracks.items.reduce((accumulator, songItem) => {
    // Generate song string
    var songString = `${songItem.artists[0].name} ${songItem.name}`

    // Add songstring only if it does not exist yet
    if (accumulator.indexOf(songString) < 0) {
      accumulator.push(songString)
    }

    return accumulator
  }, [])
}

/**
 * Uses client credentials to access spotify API
 * @see https://www.npmjs.com/package/spotify-web-api-node#client-credential-flow
 */
var access = () => {
  return new Promise((resolve, reject) => {
    // Retrieve an access token.
    spotifyApi.clientCredentialsGrant().then(
      function (data) {
        // Save the access token so that it's used in future calls
        spotifyApi.setAccessToken(data.body.access_token)
        resolve()
      },
      function (err) {
        reject(err)
      }
    )
  })
}

/**
 * Retrieves songs fitting to given words
 * @returns Array with concatted artist and songtitles
 */
var songs = (words) => {
  return new Promise((resolve, reject) => {
    // Retrieve first access to the api
    access().then(() => {
      // Search tracks for the given words
      spotifyApi.searchTracks(words)
        .then(function (data) {
          // Extract song strings and create a collection
          var extractedSongStrings = extractSongs(data)

          resolve(extractedSongStrings)
        }, function (err) {
          reject(err)
        })
    })
  })
}

/**
 * Factory method
 */
module.exports = () => {
  // Inject dependencies
  var SpotifyWebApi = require('spotify-web-api-node')

  // Configure api integration
  spotifyApi = new SpotifyWebApi({
    clientId: process.env.API_SPOTIFY_CLIENT_ID,
    clientSecret: process.env.API_SPOTIFY_CLIENT_SECRET
  })

  return {
    songs
  }
}
