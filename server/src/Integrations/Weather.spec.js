/* eslint-env mocha */
/* eslint-disable max-len */
/* eslint-disable max-lines-per-function */
var expect = require('chai').expect
var mockery = require('mockery')

beforeEach(function () {
  mockery.enable({
    warnOnReplace: true,
    warnOnUnregistered: false,
    useCleanCache: true
  })

  // Allow source under test module
  mockery.registerAllowable('./Weather')
})

afterEach(function () {
  mockery.deregisterAll()
  mockery.disable()
})

describe('Weather integration', function () {
  // Unit under test
  var weather

  // Define a stub and replace it with mockery
  var openWeatherApisStub = {
    setAPPID: () => {},
    setLang: () => {},
    setUnits: () => {},
    setCityId: () => {},
    getSmartJSON: (func) => {
      func(false, { temp: '', humidity: '', pressure: '', description: '', rain: '', weathercode: '' })
    }
  }
  mockery.registerMock('openweather-apis', openWeatherApisStub)

  beforeEach(function () {
    // Reinitialize our unit under test
    weather = require('./Weather')()
  })

  it('retrieves current weather information for Stuttgart, DE', function (done) {
    // Get current weather
    weather.currentWeather().then(result => {
      expect(result).to.be.an('object').to.have.all.keys('temp', 'humidity', 'pressure', 'description', 'rain', 'weathercode')
      done()
    }).catch(err => {
      expect.fail(err)
      done()
    })
  })

  it('retrieves a description string for current weather in Stuttgart, DE', function (done) {
    // Get a description for current weather
    weather.currentWeatherDescription().then(result => {
      expect(result).to.be.an('string')
      done()
    }).catch(err => {
      expect.fail(err)
      done()
    })
  })
})
