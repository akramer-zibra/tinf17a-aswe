/** Internal member variables */
var popyt // Youtube API integration with popyt

/**
 * Downloads audio track from given youtube url
 * @param {*} url
 * @returns {string} String with path to downloaded audio
 */
var download = (url) => {
  // Lazy load child_process dependency
  var childProcess = require('child_process')

  return new Promise((resolve, reject) => {
    try {
      // Call youtube-dl binary with exec
      /* eslint max-len: ["error", { "code": 140 }] */
      childProcess.exec(`youtube-dl --quiet --no-warnings -x --audio-format mp3 -o 'audio/song.%(ext)s' ${url}`, (err, stdout, stderr) => {
        if (err) reject(err)
        resolve(stdout)
      })
    } catch (err) {
      reject(err)
    }
  })
}

/**
 * Uses YouTube API to search videos for given words string
 * @param {string} words
 * @return {Promise<Array<any>>} Returns a promise with a array collection of simple json objects containing title, description and url
 */
var videos = (words) => {
  //
  return new Promise((resolve, reject) => {
    popyt.searchVideos(words, 10).then(result => {
      // Define result collection
      var videos = []

      // Extract title, description and url from result object
      result.results.forEach((item) => {
        videos.push({
          title: item.title,
          description: item.description,
          url: item.url
        })
      })

      resolve(videos)
    }).catch(err => reject(err))
  })
}

/**
 * Factory method
 */
module.exports = () => {
  // Require dependencies
  var { YouTube } = require('popyt')

  // Configure api instance
  popyt = new YouTube(process.env.API_YOUTUBE_KEY, { cache: false })

  return {
    download,
    videos
  }
}
