module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true
  },
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    'max-classes-per-file': ['error', 1],
    'max-depth': ['error', 4],
    'max-len': ['error', { code: 80, ignoreComments: true }],
    'max-lines': ['error', { max: 300, skipComments: true }],
    'max-lines-per-function': ['error', 30],
    'max-nested-callbacks': ['error', 10],
    'max-params': ['error', 3],
    'max-statements': ['error', 10],
    'max-statements-per-line': ['error', { max: 1 }],
    complexity: ['error', 4]
  }
}
