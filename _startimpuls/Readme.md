# Documentation with plantuml 

## Installation
`npm install -g node-plantuml`. 
You need [GraphViz](http://www.graphviz.org/) installed to be able to generate all types of image types.

## Usage
How to generate a plantuml file into an image? `puml generate file.puml -o file.png`

